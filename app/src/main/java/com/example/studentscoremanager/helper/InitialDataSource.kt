package com.example.studentscoremanager.helper

import com.example.studentscoremanager.data.local.entity.StudentEntity

object InitialDataSource {
    fun getStudents(): List<StudentEntity> {
        return listOf(
            StudentEntity(1, "Amin", "STUDENT1"),
            StudentEntity(2, "Mahendra", "STUDENT2"),
            StudentEntity(3, "Nima", "STUDENT3"),
            StudentEntity(4, "Ardneham", "STUDENT4"),
            StudentEntity(5, "Amin Mahendra", "STUDENT5"),
            StudentEntity(6, "Ardneham Nima", "STUDENT6")
        )
    }
}