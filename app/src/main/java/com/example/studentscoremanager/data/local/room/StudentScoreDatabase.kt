package com.example.studentscoremanager.data.local.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.studentscoremanager.data.local.entity.ScoreEntity
import com.example.studentscoremanager.data.local.entity.StudentEntity
import com.example.studentscoremanager.helper.InitialDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(
    entities = [StudentEntity::class, ScoreEntity::class],
    version = 1
)
abstract class StudentScoreDatabase : RoomDatabase() {

    abstract fun studentScoreDao(): StudentScoreDao

    companion object {
        @Volatile
        private var INSTANCE: StudentScoreDatabase? = null

        @JvmStatic
        fun getDatabase(context: Context, applicationScope: CoroutineScope): StudentScoreDatabase {
            if (INSTANCE == null) {
                synchronized(StudentScoreDatabase::class.java) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        StudentScoreDatabase::class.java, "student_score_database"
                    )
                        .fallbackToDestructiveMigration()
                        .addCallback(object : Callback() {
                            override fun onCreate(db: SupportSQLiteDatabase) {
                                super.onCreate(db)
                                INSTANCE?.let { database ->
                                    applicationScope.launch {
                                        val studentDao = database.studentScoreDao()
                                        studentDao.insertStudent(InitialDataSource.getStudents())
                                    }
                                }
                            }
                        })
                        .build()
                }
            }
            return INSTANCE as StudentScoreDatabase
        }
    }
}