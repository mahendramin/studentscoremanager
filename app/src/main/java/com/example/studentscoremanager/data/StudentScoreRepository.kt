package com.example.studentscoremanager.data

import androidx.lifecycle.LiveData
import com.example.studentscoremanager.data.local.entity.ScoreEntity
import com.example.studentscoremanager.data.local.room.StudentScoreDao
import com.example.studentscoremanager.domain.model.StudentScore

class StudentScoreRepository(private val studentScoreDao: StudentScoreDao) {
    fun getAllStudent(): LiveData<List<String>> = studentScoreDao.getAllStudentNames()

    fun getStudentScoreList(): LiveData<List<StudentScore>> = studentScoreDao.getStudentScoreList()

    suspend fun insertStudentScore(studentScore: ScoreEntity, name: String) {
        studentScore.studentID = studentScoreDao.getStudentID(name)
        studentScoreDao.insertScore(studentScore)
    }

    suspend fun editStudentScore(
        assignmentScore: Float,
        midtermexamScore: Float,
        finalExamScore: Float,
        score: Float,
        scoreIndex: String,
        studentID: String
    ) {
        studentScoreDao.editStudentScore(
            assignmentScore,
            midtermexamScore,
            finalExamScore,
            score,
            scoreIndex,
            studentID
        )
    }

    suspend fun deleteStudentScore(studentID: String) {
        studentScoreDao.deleteStudentScore(studentID)
    }
}