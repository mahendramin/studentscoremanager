package com.example.studentscoremanager.data.local.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.studentscoremanager.data.local.entity.ScoreEntity
import com.example.studentscoremanager.data.local.entity.StudentEntity
import com.example.studentscoremanager.domain.model.StudentScore

@Dao
interface StudentScoreDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertStudent(studentList: List<StudentEntity>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertScore(score: ScoreEntity)

    @Query(
        """
            SELECT name, score.student_id, assignment_score, midtermexam_score, finalexam_score, score, score_index from score
            JOIN student
            ON score.student_id = student.student_id
            ORDER BY score.student_id ASC
    """
    )
    fun getStudentScoreList(): LiveData<List<StudentScore>>

    @Query(
        """
            select name from student 
            left join score
            on student.student_id = score.student_id
            where student.student_id not in (select student_id from score)
        """
    )
    fun getAllStudentNames(): LiveData<List<String>>

    @Query(
        """
            select student.student_id from student
            where name = :name
        """
    )
    suspend fun getStudentID(name: String): String

    @Query(
        """
        update score set 
        assignment_score = :assignmentScore, midtermexam_score = :midtermexamScore, 
        finalexam_score = :finalExamScore, score = :score, score_index = :scoreIndex
        where student_id = :studentID
    """
    )
    suspend fun editStudentScore(
        assignmentScore: Float,
        midtermexamScore: Float,
        finalExamScore: Float,
        score: Float,
        scoreIndex: String,
        studentID: String
    )

    @Query("DELETE FROM score where student_id = :studentID")
    suspend fun deleteStudentScore(studentID: String)
}