package com.example.studentscoremanager.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "score")
class ScoreEntity {
    @field:ColumnInfo(name = "id")
    @field:PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @field:ColumnInfo(name = "student_id")
    var studentID: String = String()

    @field:ColumnInfo(name = "assignment_score")
    var assignmentScore: Float = 0f

    @field:ColumnInfo(name = "midtermexam_score")
    var midtermExamScore: Float = 0f

    @field:ColumnInfo(name = "finalexam_score")
    var finaltermExamScore: Float = 0f

    @field:ColumnInfo(name = "score")
    var score: Float = 0f

    @field:ColumnInfo(name = "score_index")
    var scoreIndex: String = String()
}