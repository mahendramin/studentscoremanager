package com.example.studentscoremanager.data.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "student")
class StudentEntity(
    @field:ColumnInfo(name = "id")
    @field:PrimaryKey(autoGenerate = true)
    var id: Int = 0,

    @field:ColumnInfo(name = "name")
    var name: String,

    @field:ColumnInfo(name = "student_id")
    var studentID: String
)