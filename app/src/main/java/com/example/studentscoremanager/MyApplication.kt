package com.example.studentscoremanager

import android.app.Application
import com.example.studentscoremanager.data.StudentScoreRepository
import com.example.studentscoremanager.data.local.room.StudentScoreDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class MyApplication : Application() {
    private val applicationScope = CoroutineScope(SupervisorJob())
    private val database by lazy { StudentScoreDatabase.getDatabase(this, applicationScope) }
    val repository by lazy { StudentScoreRepository(database.studentScoreDao()) }
}