package com.example.studentscoremanager.ui

import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import com.example.studentscoremanager.MyApplication
import com.example.studentscoremanager.data.local.entity.ScoreEntity
import com.example.studentscoremanager.databinding.ActivityEditScoreBinding
import com.example.studentscoremanager.domain.model.StudentScore
import com.example.studentscoremanager.ui.viewmodel.MainViewModel
import com.example.studentscoremanager.ui.viewmodel.ViewModelFactory
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class EditScoreActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEditScoreBinding
    private val mainViewModel: MainViewModel by viewModels {
        ViewModelFactory((application as MyApplication).repository)
    }
    private var studentId = String()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditScoreBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val studentScore = if (Build.VERSION.SDK_INT >= 33) {
            intent.getParcelableExtra(STUDENT_DATA, StudentScore::class.java)
        } else {
            @Suppress("DEPRECATION")
            intent.getParcelableExtra(STUDENT_DATA)
        }

        studentScore?.let {
            binding.etDropdown.setText(it.name)
            binding.etAssignmentScoreValue.setText(it.assignmentScore.toString())
            binding.etMidTermScoreValue.setText(it.midtermExamScore.toString())
            binding.etFinalExamScoreValue.setText(it.finalExamScore.toString())
            studentId = it.studentID
        }


        binding.etAssignmentScoreValue.addTextChangedListener {
            if (it.toString().isNotBlank()) {
                if (it.toString().toFloat() > 100) {
                    binding.tilAssignmentScoreValue.error = "Score must no be higher than 100"
                    binding.tilAssignmentScoreValue.isErrorEnabled = true
                } else {
                    binding.tilAssignmentScoreValue.error = null
                    binding.tilAssignmentScoreValue.isErrorEnabled = false
                }
            }
        }

        binding.etMidTermScoreValue.addTextChangedListener {
            if (it.toString().isNotBlank()) {
                if (it.toString().toFloat() > 100) {
                    binding.tilMidTermScoreValue.error = "Score must no be higher than 100"
                    binding.tilMidTermScoreValue.isErrorEnabled = true
                } else {
                    binding.tilMidTermScoreValue.error = null
                    binding.tilMidTermScoreValue.isErrorEnabled = false
                }
            }
        }

        binding.etFinalExamScoreValue.addTextChangedListener {
            if (it.toString().isNotBlank()) {
                if (it.toString().toFloat() > 100) {
                    binding.tilFinalExamScoreValue.error = "Score must no be higher than 100"
                    binding.tilFinalExamScoreValue.isErrorEnabled = true
                } else {
                    binding.tilFinalExamScoreValue.error = null
                    binding.tilFinalExamScoreValue.isErrorEnabled = false
                }
            }
        }

        binding.btnEdit.setOnClickListener {
            if (
                binding.etDropdown.text.toString().isNotBlank() &&
                (binding.etAssignmentScoreValue.text.toString()
                    .isNotBlank() && binding.tilAssignmentScoreValue.error == null) &&
                (binding.etMidTermScoreValue.text.toString()
                    .isNotBlank() && binding.tilMidTermScoreValue.error == null) &&
                (binding.etFinalExamScoreValue.text.toString()
                    .isNotBlank() && binding.tilFinalExamScoreValue.error == null)
            ) {


                val assignmentScore = binding.etAssignmentScoreValue.text.toString().toFloat()
                val midTermScore = binding.etMidTermScoreValue.text.toString().toFloat()
                val finalExamScore = binding.etFinalExamScoreValue.text.toString().toFloat()
                val score =
                    (((assignmentScore + midTermScore) * 0.3) + (finalExamScore * 0.4)).toFloat()
                val scoreEntity = ScoreEntity()
                scoreEntity.studentID = studentId
                scoreEntity.assignmentScore = assignmentScore
                scoreEntity.midtermExamScore = midTermScore
                scoreEntity.finaltermExamScore = finalExamScore
                scoreEntity.score = score
                scoreEntity.scoreIndex = when {
                    score >= 85 -> "A"
                    score >= 80 -> "AB"
                    score >= 70 -> "B"
                    score >= 65 -> "BC"
                    score >= 60 -> "C"
                    score >= 50 -> "D"
                    else -> "E"
                }
                mainViewModel.editStudentScore(
                    scoreEntity
                )
                finish()
            } else {
                Toast.makeText(this, "Please check your data", Toast.LENGTH_SHORT).show()
            }
        }

        binding.btnDelete.setOnClickListener {
            val materialAlertDialogBuilder = MaterialAlertDialogBuilder(this)
            materialAlertDialogBuilder.setTitle("Delete student score data?")
            materialAlertDialogBuilder.setPositiveButton("Yes") { _, _ ->
                mainViewModel.deleteStudentScore(studentId)
                finish()
            }
            materialAlertDialogBuilder.setNegativeButton("No") { _, _ ->

            }
            materialAlertDialogBuilder.show()
        }
    }

    companion object {
        const val STUDENT_DATA = "student_data"
    }
}