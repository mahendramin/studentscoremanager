package com.example.studentscoremanager.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.studentscoremanager.databinding.ItemStudentScoreBinding
import com.example.studentscoremanager.domain.model.StudentScore

class StudentScoreListAdapter(private val onClick: (StudentScore) -> Unit) :
    ListAdapter<StudentScore, StudentScoreListAdapter.WordViewHolder>(WordsComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val binding =
            ItemStudentScoreBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return WordViewHolder(binding)
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class WordViewHolder(private val binding: ItemStudentScoreBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: StudentScore) {
            with(binding) {
                with(data) {
                    tvName.text = name
                    tvStudentID.text = studentID
                    tvFinalScore.text = "Score: $score ($scoreIndex)"
                    tvMidTermScoreValue.text = midtermExamScore.toString()
                    tvFinalExamScoreValue.text = finalExamScore.toString()
                    tvAssignmentScoreValue.text = assignmentScore.toString()
                    itemView.setOnClickListener {
                        onClick(data)
                    }
                }
            }
        }
    }

    class WordsComparator : DiffUtil.ItemCallback<StudentScore>() {
        override fun areItemsTheSame(oldItem: StudentScore, newItem: StudentScore): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: StudentScore, newItem: StudentScore): Boolean {
            return oldItem.name == newItem.name
        }
    }
}