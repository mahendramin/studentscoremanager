package com.example.studentscoremanager.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.studentscoremanager.MyApplication
import com.example.studentscoremanager.databinding.ActivityMainBinding
import com.example.studentscoremanager.ui.adapter.StudentScoreListAdapter
import com.example.studentscoremanager.ui.viewmodel.MainViewModel
import com.example.studentscoremanager.ui.viewmodel.ViewModelFactory

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModels {
        ViewModelFactory((application as MyApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getStudentScoreList()
        binding.fab.setOnClickListener {
            startActivity(Intent(this, InputScoreActivity::class.java))
        }
    }

    private fun getStudentScoreList() {
        val adapter = StudentScoreListAdapter{
            val editScoreIntent = Intent(this, EditScoreActivity::class.java)
            editScoreIntent.putExtra(EditScoreActivity.STUDENT_DATA, it)
            startActivity(editScoreIntent)
        }
        binding.rvStudentScore.adapter = adapter
        mainViewModel.getStudentScoreList().observe(this) {
            if (it.isNotEmpty()) {
                showEmptyList(false)
                adapter.submitList(it)
            } else {
                showEmptyList(true)
            }
        }
    }

    private fun showEmptyList(isListEmpty: Boolean) {
        if (isListEmpty) {
            binding.tvEmptyList.visibility = View.VISIBLE
            binding.rvStudentScore.visibility = View.GONE
        } else {
            binding.tvEmptyList.visibility = View.GONE
            binding.rvStudentScore.visibility = View.VISIBLE
        }
    }

    private companion object {
        private const val TAG = "MainActivity"
    }
}