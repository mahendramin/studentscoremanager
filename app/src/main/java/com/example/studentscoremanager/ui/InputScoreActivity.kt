package com.example.studentscoremanager.ui

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import com.example.studentscoremanager.MyApplication
import com.example.studentscoremanager.data.local.entity.ScoreEntity
import com.example.studentscoremanager.databinding.ActivityInputScoreBinding
import com.example.studentscoremanager.ui.viewmodel.MainViewModel
import com.example.studentscoremanager.ui.viewmodel.ViewModelFactory
import com.google.android.material.textfield.MaterialAutoCompleteTextView

class InputScoreActivity : AppCompatActivity() {

    private lateinit var binding: ActivityInputScoreBinding
    private val mainViewModel: MainViewModel by viewModels {
        ViewModelFactory((application as MyApplication).repository)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInputScoreBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.etAssignmentScoreValue.addTextChangedListener {
            if (it.toString().isNotBlank()) {
                if (it.toString().toFloat() > 100) {
                    binding.tilAssignmentScoreValue.error = "Score must no be higher than 100"
                    binding.tilAssignmentScoreValue.isErrorEnabled = true
                } else {
                    binding.tilAssignmentScoreValue.error = null
                    binding.tilAssignmentScoreValue.isErrorEnabled = false
                }
            }
        }

        binding.etMidTermScoreValue.addTextChangedListener {
            if (it.toString().isNotBlank()) {
                if (it.toString().toFloat() > 100) {
                    binding.tilMidTermScoreValue.error = "Score must no be higher than 100"
                    binding.tilMidTermScoreValue.isErrorEnabled = true
                } else {
                    binding.tilMidTermScoreValue.error = null
                    binding.tilMidTermScoreValue.isErrorEnabled = false
                }
            }
        }

        binding.etFinalExamScoreValue.addTextChangedListener {
            if (it.toString().isNotBlank()) {
                if (it.toString().toFloat() > 100) {
                    binding.tilFinalExamScoreValue.error = "Score must no be higher than 100"
                    binding.tilFinalExamScoreValue.isErrorEnabled = true
                } else {
                    binding.tilFinalExamScoreValue.error = null
                    binding.tilFinalExamScoreValue.isErrorEnabled = false
                }
            }
        }

        mainViewModel.getAllStudent().observe(this) {
            if (it.isNotEmpty()) {
                val x = mutableListOf<String>()
                it.forEach { name ->
                    x.add(name)
                }
                (binding.tilDropdown.editText as? MaterialAutoCompleteTextView)?.setSimpleItems(
                    x.toTypedArray()
                )
            } else {
                binding.btnInput.isEnabled = false
                Toast.makeText(
                    this,
                    "All of student score is already inputted",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }

        }

        binding.btnInput.setOnClickListener {
            if (
                binding.etDropdown.text.toString().isNotBlank() &&
                (binding.etAssignmentScoreValue.text.toString()
                    .isNotBlank() && binding.tilAssignmentScoreValue.error == null) &&
                (binding.etMidTermScoreValue.text.toString()
                    .isNotBlank() && binding.tilMidTermScoreValue.error == null) &&
                (binding.etFinalExamScoreValue.text.toString()
                    .isNotBlank() && binding.tilFinalExamScoreValue.error == null)
            ) {


                val assignmentScore = binding.etAssignmentScoreValue.text.toString().toFloat()
                val midTermScore = binding.etMidTermScoreValue.text.toString().toFloat()
                val finalExamScore = binding.etFinalExamScoreValue.text.toString().toFloat()
                val score =
                    (((assignmentScore + midTermScore) * 0.3) + (finalExamScore * 0.4)).toFloat()
                val scoreEntity = ScoreEntity()
                scoreEntity.assignmentScore = assignmentScore
                scoreEntity.midtermExamScore = midTermScore
                scoreEntity.finaltermExamScore = finalExamScore
                scoreEntity.score = score
                scoreEntity.scoreIndex = when {
                    score >= 85 -> "A"
                    score >= 80 -> "AB"
                    score >= 70 -> "B"
                    score >= 65 -> "BC"
                    score >= 60 -> "C"
                    score >= 50 -> "D"
                    else -> "E"
                }
                mainViewModel.insertStudentScore(
                    scoreEntity, binding.etDropdown.text.toString()
                )
                finish()
            } else  {
                Toast.makeText(this, "Please check your data", Toast.LENGTH_SHORT).show()
            }
        }
    }
}