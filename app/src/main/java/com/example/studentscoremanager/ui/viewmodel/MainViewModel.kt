package com.example.studentscoremanager.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.studentscoremanager.data.StudentScoreRepository
import com.example.studentscoremanager.data.local.entity.ScoreEntity
import com.example.studentscoremanager.domain.model.StudentScore
import kotlinx.coroutines.launch

class MainViewModel(private val studentScoreRepository: StudentScoreRepository) : ViewModel() {
    fun getAllStudent(): LiveData<List<String>> = studentScoreRepository.getAllStudent()

    fun getStudentScoreList(): LiveData<List<StudentScore>> =
        studentScoreRepository.getStudentScoreList()

    fun insertStudentScore(scoreEntity: ScoreEntity, name: String) {
        viewModelScope.launch {
            studentScoreRepository.insertStudentScore(scoreEntity, name)
        }
    }

    fun editStudentScore(studentScore: ScoreEntity) {
        viewModelScope.launch {
            studentScoreRepository.editStudentScore(
                studentScore.assignmentScore,
                studentScore.midtermExamScore,
                studentScore.finaltermExamScore,
                studentScore.score,
                studentScore.scoreIndex,
                studentScore.studentID
            )
        }
    }

    fun deleteStudentScore(studentID: String) {
        viewModelScope.launch {
            studentScoreRepository.deleteStudentScore(studentID)
        }
    }
}

class ViewModelFactory(private val repository: StudentScoreRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return MainViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}