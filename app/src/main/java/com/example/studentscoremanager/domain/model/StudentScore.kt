package com.example.studentscoremanager.domain.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import kotlinx.parcelize.Parcelize

@Parcelize
class StudentScore(
    var name: String,
    @ColumnInfo("student_id")
    var studentID: String,
    @ColumnInfo("assignment_score")
    var assignmentScore: Float,
    @ColumnInfo("midtermexam_score")
    var midtermExamScore: Float,
    @ColumnInfo("finalexam_score")
    var finalExamScore: Float,
    @ColumnInfo("score")
    var score: Float,
    @ColumnInfo("score_index")
    var scoreIndex: String,
): Parcelable